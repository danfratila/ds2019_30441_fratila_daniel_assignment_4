
package com.example.whatever.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.example.whatever.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetTestModelResponse_QNAME = new QName("http://soap.whatever.example.com/", "getTestModelResponse");
    private final static QName _GetTestModel_QNAME = new QName("http://soap.whatever.example.com/", "getTestModel");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.example.whatever.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetTestModel }
     * 
     */
    public GetTestModel createGetTestModel() {
        return new GetTestModel();
    }

    /**
     * Create an instance of {@link GetTestModelResponse }
     * 
     */
    public GetTestModelResponse createGetTestModelResponse() {
        return new GetTestModelResponse();
    }

    /**
     * Create an instance of {@link TestModel }
     * 
     */
    public TestModel createTestModel() {
        return new TestModel();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTestModelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "getTestModelResponse")
    public JAXBElement<GetTestModelResponse> createGetTestModelResponse(GetTestModelResponse value) {
        return new JAXBElement<GetTestModelResponse>(_GetTestModelResponse_QNAME, GetTestModelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTestModel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "getTestModel")
    public JAXBElement<GetTestModel> createGetTestModel(GetTestModel value) {
        return new JAXBElement<GetTestModel>(_GetTestModel_QNAME, GetTestModel.class, null, value);
    }

}
