package com.example.whatever.rmi;

import com.example.whatever.entities.MedicationPlan;

import java.rmi.RemoteException;
import java.time.LocalTime;
import java.util.List;


public interface RMIServiceInterface  {

    public LocalTime getCurrentTime();

    public List<MedicationPlan> getTodayMedicationPlan(int id);

    public void setMedicationTaken(MedicationPlan medicationPlan) throws RemoteException;

    public void setMedicationNotTaken(MedicationPlan medicationPlan) throws RemoteException;
}
