package com.example.whatever.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.whatever.dtos.MedicationDTO;
import com.example.whatever.entities.Medication;
import com.example.whatever.repos.MedicationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class MedicationService {

	@Autowired
	private MedicationRepo medicationRepository;


	public MedicationDTO findMedicationById(int id) {
		Optional<Medication> med = medicationRepository.findById(id);
		if (!med.isPresent()) {
			//throw new ResourceNotFoundException(Medication.class.getSimpleName());
			return new MedicationDTO();
		}
		else{
			return new MedicationDTO.Builder()
					.id(med.get().getId())
					.name(med.get().getName())
					.sideEffects(med.get().getSideEffects())
					.dosage(med.get().getDosage())
					.create();
		}
	}


	public List<MedicationDTO> findMedicationByName(String name) {
		List<MedicationDTO> returnDtos = new ArrayList<>();
		List<Medication> meds = medicationRepository.findAll();
		for(Medication med: meds) {
			if(med.getName().toLowerCase().contains(name.toLowerCase())) {
				MedicationDTO dto = new MedicationDTO.Builder()
						.id(med.getId())
						.name(med.getName())
						.sideEffects(med.getSideEffects())
						.dosage(med.getDosage())
						.create();
				returnDtos.add(dto);
			}
		}
		return returnDtos;
	}


	public List<MedicationDTO> findAll() {
		List<Medication> meds = medicationRepository.findAll();
		List<MedicationDTO> toReturn = new ArrayList<>();
		for (Medication med: meds) {
			MedicationDTO dto = new MedicationDTO.Builder()
						.id(med.getId())
						.name(med.getName())
						.sideEffects(med.getSideEffects())
						.dosage(med.getDosage())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(MedicationDTO medDTO) {
		Medication medication = new Medication();
		
		medication.setName(medDTO.getName());
		medication.setSideEffects(medDTO.getSideEffects());
		medication.setDosage(medDTO.getDosage());
		
		Medication med = medicationRepository.save(medication);
		return med.getId();
	}
	

	public void update(String name, MedicationDTO medDTO) {
		Optional<Medication> med = medicationRepository.findByName(name);
		if(med.isPresent()) {
			med.get().setName(medDTO.getName()); // is the same
			if(medDTO.getSideEffects() != null && !medDTO.getSideEffects().equals("")) {
				med.get().setSideEffects(medDTO.getSideEffects());
			}
			if(medDTO.getDosage() != null) {
				med.get().setDosage(medDTO.getDosage());
			}
			medDTO.setId(med.get().getId());
			medicationRepository.save(med.get());
		}
		//return medDTO;
	}
	
	
	public MedicationDTO delete(String name) {
		Optional<Medication> med = medicationRepository.findByName(name);
		med.ifPresent(medication -> medicationRepository.delete(medication));
		return new MedicationDTO();
	}
	

}
