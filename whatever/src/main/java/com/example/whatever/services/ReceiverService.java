package com.example.whatever.services;

import com.example.whatever.dtos.PatientDataDTO;
import com.example.whatever.entities.PatientData;
import com.example.whatever.event.BaseEvent;
import com.example.whatever.event.NotificationCreatedEvent;
import com.example.whatever.repos.PatientDataRepo;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;


@Component
@RequiredArgsConstructor
public class ReceiverService {

    private static final String QUEUE_NAME = "q";
    private static final long HOUR_TO_MILLIS = 3600 * 1000;
    private static final long MAX_SLEEP_HOURS = 12 * HOUR_TO_MILLIS;
    private static final long MAX_LEAVE_HOURS = 12 * HOUR_TO_MILLIS;
    private static final long MAX_TOILET_HOURS = 1 * HOUR_TO_MILLIS;
    private static final String SLEEPING_ACTIVITY = "Sleeping";
    private static final String LEAVING_ACTIVITY = "Leaving";
    private static final String TOILETING_ACTIVITY = "Toileting";

    @Autowired
    private PatientDataRepo patientDataRepository;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private ApplicationEventPublisher eventPublisher;


    @EventListener(BaseEvent.class)
    public void handleEvent(BaseEvent event){
        System.out.println("------------------Got event " + event);
        messagingTemplate.convertAndSend("/topic/events", event);
    }


    public void establishConnection() throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            JSONParser jsonParser =  new JSONParser();
            try {
                List<JSONObject> receivedData = (ArrayList<JSONObject>)jsonParser.parse(message);
                receivedData.forEach(element -> {
                    PatientData newData = new PatientData(
                            element.get("patient_id").toString(),
                            element.get("activity").toString(),
                            Long.parseLong(element.get("start").toString()),
                            Long.parseLong(element.get("end").toString()));

                    checkAllOk(newData);
                });
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }


    private void checkAllOk(PatientData patientData){

        String badActivity = "";
        long activityDuration = patientData.getEndTime() - patientData.getStartTime();

        if (patientData.getActivity().contains(SLEEPING_ACTIVITY) && activityDuration > MAX_SLEEP_HOURS) {
            badActivity = SLEEPING_ACTIVITY;
        } else if (patientData.getActivity().contains(LEAVING_ACTIVITY) && activityDuration > MAX_LEAVE_HOURS) {
            badActivity = LEAVING_ACTIVITY;
        } else if (patientData.getActivity().contains(TOILETING_ACTIVITY) && activityDuration > MAX_TOILET_HOURS) {
            badActivity = TOILETING_ACTIVITY;
        }
        PatientData p = patientDataRepository.save(patientData); // always

        if(!badActivity.equals("")){
            System.out.println("\tTOO MUCH " + badActivity + "; Patient: " + patientData.toString());
            PatientDataDTO newPatientDataDTO = new PatientDataDTO(p.getId(), p.getPatient_id(), p.getActivity(), p.getStartTime().toString(), p.getEndTime().toString()); // create
            eventPublisher.publishEvent(new NotificationCreatedEvent(newPatientDataDTO));
        }
    }

    public List<PatientDataDTO> findAll() throws IOException, TimeoutException {
        establishConnection();
        List<PatientData> patientsInfos = patientDataRepository.findAll();
        List<PatientDataDTO> toReturn = new ArrayList<>();
        for (PatientData patientData: patientsInfos) {
            toReturn.add(new PatientDataDTO(patientData.getId(),
                    patientData.getPatient_id(),
                    patientData.getActivity(),
                    patientData.getStartTime().toString(),
                    patientData.getEndTime().toString()));
        }
        return toReturn;
    }
}
