package com.example.whatever.controllers;

import java.util.List;

import com.example.whatever.dtos.MedicationDTO;
import com.example.whatever.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/medication")
public class MedicationController {

	@Autowired
	private MedicationService medicationService;

	// create -- ok
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertMedication(@RequestBody MedicationDTO medicationDTO) {
		return medicationService.create(medicationDTO);
	}
	
	// read - filter by name -- ok
	@RequestMapping(value = "/details/{name}", method = RequestMethod.GET)
	public List<MedicationDTO> getMedicationByName(@PathVariable("name") String name) {
		return medicationService.findMedicationByName(name);
	}

	// read all -- ok
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<MedicationDTO> getAllMedications() {
		return medicationService.findAll();
	}

	// update -- ok
	@RequestMapping(value = "/update/{name}", method = RequestMethod.PUT)
	public void updateMedication(@PathVariable("name") String name, @RequestBody MedicationDTO medicationDTO) {		
		 medicationService.update(name, medicationDTO);
	}
	
	// delete -- ok
	@RequestMapping(value = "/delete/{name}", method = RequestMethod.DELETE)
	public MedicationDTO deleteMedication(@PathVariable("name") String name) {
		return medicationService.delete(name);
	}
}

