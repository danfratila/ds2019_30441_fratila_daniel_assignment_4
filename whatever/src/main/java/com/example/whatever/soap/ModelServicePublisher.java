package com.example.whatever.soap;

import javax.xml.ws.Endpoint;

public class ModelServicePublisher {
    public static void main(String[] args){
        Endpoint.publish("http://localhost:8081/testsoap", new ModelServiceImpl());
    }
}
