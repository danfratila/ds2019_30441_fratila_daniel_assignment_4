package com.example.whatever.dtos;


public class PatientDataDTO {

    private Integer id;
    private String patient_id;
    private String activity;
    private String startTime;
    private String endTime;


    public PatientDataDTO() {
    }


    public PatientDataDTO(Integer id, String patient_id, String activity, String startTime, String endTime){
        this.id = id;
        this.patient_id = patient_id;
        this.activity = activity;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }


    public static class Builder {
        private Integer nestedid;
        private String nestedpatientid;
        private String nestedactivity;
        private String nestedstarttime;
        private String nestedendtime;

        public Builder id(int id) {
            this.nestedid = id;
            return this;
        }

        public Builder patient_id(String id) {
            this.nestedpatientid = id;
            return this;
        }

        public Builder activity(String nestedactivity) {
            this.nestedactivity = nestedactivity;
            return this;
        }

        public Builder starttime(String nestedstarttime) {
            this.nestedstarttime = nestedstarttime;
            return this;
        }

        public Builder endtime(String nestedendtime) {
            this.nestedendtime = nestedendtime;
            return this;
        }

        public PatientDataDTO create() {
            return new PatientDataDTO(nestedid, nestedpatientid, nestedactivity, nestedstarttime, nestedendtime);
        }

    }

}