package com.example.whatever;

import com.example.whatever.soap.ModelServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import javax.xml.ws.Endpoint;


@SpringBootApplication
public class WhateverApplication {

	@Autowired
	private ModelServiceImpl modelService;

	@EventListener(ApplicationReadyEvent.class)
	public void publishWebService() {
		Endpoint.publish("http://localhost:8081/testsoap", modelService);
		System.out.println("am publicat");
	}

	public static void main(String[] args) {
		SpringApplication.run(WhateverApplication.class, args);
	}

}
