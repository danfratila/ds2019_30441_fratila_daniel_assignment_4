package com.example.whatever.entities;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name = "taken")
public class TakenMedicationPlan implements java.io.Serializable{

    private Integer id;
    private Integer patient;
    private String medication;
    private Integer interval;
    private Integer period;

    public TakenMedicationPlan() {
    }

    public TakenMedicationPlan(Integer id, Integer patient, String medication, Integer interval, Integer period) {
        super();
        this.id = id;
        this.patient = patient;
        this.medication = medication;
        this.interval = interval;
        this.period = period;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "patient")
    public Integer getPatient() {
        return this.patient;
    }

    public void setPatient(Integer patient) {
        this.patient = patient;
    }

    @Column(name = "medication")
    public String getMedication() {
        return this.medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    // in minutes
    @Column(name = "interval")
    public Integer getInterval() {
        return this.interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    // in days
    @Column(name = "period")
    public Integer getPeriod() {
        return this.period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }


    @Override
    public String toString(){
        return id + "; " + patient + "; " + medication + "; " + period + "; " + interval + '\n';
    }
}
