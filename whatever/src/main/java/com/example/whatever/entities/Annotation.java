package com.example.whatever.entities;

import javax.persistence.*;


@Entity
@Table(name = "annotations")
public class Annotation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer patientId;
    private Integer activityId;
    private String annotation;
    private String recommendation;

    public Annotation(){

    }

    public Annotation(Integer patientId, Integer activityId, String annotation, String recommendation) {
        this.patientId = patientId;
        this.activityId = activityId;
        this.annotation = annotation;
        this.recommendation = recommendation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public String getIsNormal(){ return annotation;}

    public void setIsNormal(String annotation){ this.annotation = annotation;}

    public String getRecommendation() { return recommendation;}

    public void setRecommendation(String recommendation) { this.recommendation = recommendation;}
}