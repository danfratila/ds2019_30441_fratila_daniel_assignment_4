package com.example.soap.controller;

import com.example.soap.service.SoapService;
import com.example.soap.soappkg.AnnotationDTO;
import com.example.soap.soappkg.PatientDataDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/doctor")
public class TestSoapController {

    @Autowired
    private SoapService soapService;

    @RequestMapping(value = "/viewdata/{id}", method = RequestMethod.GET)
    public void viewActivityHistory(@PathVariable("id") int id) {
        soapService.viewActivityHistory(id);
    }

    @RequestMapping(value = "/viewdata/activities/names", method = RequestMethod.GET)
    public List<String> viewActivityNames() {
        return soapService.viewActivityNames();
    }

    @RequestMapping(value = "/viewdata/activities/data", method = RequestMethod.GET)
    public List<Long> viewActivityData() {
        return soapService.viewActivityData();
    }
    // GATA 1

    @RequestMapping(value = "/viewdata/activities/abnormals", method = RequestMethod.GET)
    public List<PatientDataDTO> viewAbnormalActivities() {
        return soapService.viewAbnormalActivities();
    }


    @RequestMapping(value = "/viewplan/{id}/{date}", method = RequestMethod.GET)
    public void viewTakenPlanAtDay(@PathVariable("id") int id, @PathVariable("date") String date) {
        soapService.viewTakenPlanAtDay(id, date);
    }

    @RequestMapping(value = "/annotation/insert", method = RequestMethod.POST)
    public void annotatePatientBehavior(@RequestBody AnnotationDTO annotationDTO) {
        System.out.println("ajung: " + annotationDTO.getActivityId());
        soapService.annotatePatientBehavior(annotationDTO);
    }

    @RequestMapping(value = "/recommendation/insert", method = RequestMethod.POST)
    public void addRecommendation(@RequestBody AnnotationDTO annotationDTO){
        soapService.addRecommendation(annotationDTO);
    }
}
