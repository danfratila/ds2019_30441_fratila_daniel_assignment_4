package com.example.soap.service;

import com.example.soap.soappkg.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SoapService {

    private ModelService modelService;

    public SoapService(){
        URL url = null;
        try {
            url = new URL("http://localhost:8081/testsoap?wsdl");
         } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        ModelServiceImplService serviceImplService = new ModelServiceImplService(url);
        modelService = serviceImplService.getModelServiceImplPort();
    }

    public void viewActivityHistory(int patientId){
        modelService.viewActivityHistory(patientId);
    }

    public List<String> viewActivityNames(){
        return modelService.viewActivityNames();
    }

    public List<Long> viewActivityData(){
        List<Long> datas = new ArrayList<>();
        List<String> names = modelService.viewActivityNames();
        for(String iterator: names){
            datas.add(modelService.viewDataForActivity(iterator));
            System.out.println("-----" + iterator);
        }
        for(Long iterator: datas){
            System.out.println(iterator);

        }
        return datas;
    }
    // ------------------------------ (1)


    public List<PatientDataDTO> viewAbnormalActivities(){
        return modelService.viewAbnormalActivities(1);
    }


    public void viewTakenPlanAtDay(int patientId, String date){
        modelService.viewTakenPlanAtDay(patientId, date);
    }

    public void annotatePatientBehavior(AnnotationDTO annotationDTO){
        modelService.annotatePatientBehavior(annotationDTO);
    }

    public void addRecommendation(AnnotationDTO annotationDTO){
        modelService.addRecommendation(annotationDTO);
    }


//    public void runService()throws Exception{
//        URL url = new URL("http://localhost:8081/testsoap?wsdl");
//        ModelServiceImplService serviceImplService = new ModelServiceImplService(url);
//        ModelService modelService = serviceImplService.getModelServiceImplPort();
//        modelService.viewActivityHistory(0);
//    }
}
