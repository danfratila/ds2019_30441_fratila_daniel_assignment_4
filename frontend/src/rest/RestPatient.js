const BASE_URL = "http://localhost:8080/patient";
const BASE_URL_SOAP = "http://localhost:8082/doctor";

export default class RestPatient {

    createAnnotation(id, text){
        return fetch(BASE_URL_SOAP + "/annotation/insert", {
            method: "POST",
            body: JSON.stringify({
                activityId: id,
                annotation: text
            }),
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        }).then(response =>{
            //return response.json()
        });
    }

    createRecommendation(id, text){
        return fetch(BASE_URL_SOAP + "/recommendation/insert", {
            method: "POST",
            body: JSON.stringify({
                activityId: id,
                recommendation: text
            }),
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        }).then(response =>{
            //return response.json()
        });
    }

    loadAbnormalActivities(){
        return fetch( BASE_URL_SOAP + "/viewdata/activities/abnormals", {
            method: "GET",
            headers: {
                //"Authorization": this.authorization

            }
        }).then(response => {
            return response.json()});
    }

    loadDataForPatient1(){
        return fetch( "http://localhost:8080/patientdata/all", {
            method: "GET",
            headers: {
                //"Authorization": this.authorization

            }
        }).then(response => {
            return response.json()});
    }

    loadActivitiesForPatient1Charts(){
        return fetch( BASE_URL_SOAP + "/viewdata/activities/names", {
            method: "GET",
            headers: {
                //"Authorization": this.authorization

            }
        }).then(response => {
            return response.json()});
    }

    loadDataForPatient1Charts(){
        return fetch( BASE_URL_SOAP + "/viewdata/activities/data", {
            method: "GET",
            headers: {
                //"Authorization": this.authorization

            }
        }).then(response => {
            return response.json()});
    }

    loadAllPatients() {
        return fetch(BASE_URL + "/all", {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            return response.json()});
    }

    createPatient(firstName, lastName, birthdate, gender, address, record){
        return fetch(BASE_URL + "/insert", {
            method: "POST",
            body: JSON.stringify({
                firstname: firstName,
                surname: lastName,
                birthdate: birthdate,
                gender: gender,
                address: address,
                record: record
            }),
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        }).then(response =>{
            return response.json()
        });
    }



    loadCertainPatientByName(name) {
        return fetch(BASE_URL + "/details/" + name, {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            return response.json()});
    }

    loadCertainPatients(username){
        return fetch(BASE_URL + "/details/specific/" + username, {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            return response.json()});
    }

    loadCertainPatientById(id){
        return fetch(BASE_URL + "/details/" + id, {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            return response.json()});
    }

    updatePatient(patientId, firstname, lastname, birthdate, gender, address, record){
        return fetch(BASE_URL + "/update/" + patientId, {
            method: "PUT",
            body: JSON.stringify({
                id: patientId,
                firstname: firstname,
                surname: lastname,
                birthdate: birthdate,
                gender: gender,
                address: address,
                record: record
            }),
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        }).then(response => {
            //return response.json()
            });
    }

    deletePatientById(id){
        return fetch(BASE_URL + "/delete/" + id, {
            method: "DELETE",
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        }).then(response => {
            //return response.json()}
        });
    }
}
