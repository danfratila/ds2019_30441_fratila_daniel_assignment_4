import caregiverModel from "../../model/caregiverModel";

class DeleteCaregiverPresenter {
    onDelete(){
        caregiverModel.deleteCaregiver(caregiverModel.state.deleteId)
            .then(() => {
                caregiverModel.changeDeleteCaregiverProperty("");
                caregiverModel.loadCaregivers();
            });
    }

    onChangeDeleteId(value){
        caregiverModel.changeDeleteCaregiverProperty(value);
    }
}

const deleteCaregiverPresenter = new DeleteCaregiverPresenter();

export default deleteCaregiverPresenter;
