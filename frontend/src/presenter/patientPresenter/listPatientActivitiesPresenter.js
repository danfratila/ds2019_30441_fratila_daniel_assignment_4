import patientModel from "../../model/patientModel";


class ListPatientActivitiesPresenter {
    onInit() {
        patientModel.loadPatients();
        patientModel.loadActivitiesCharts();
        patientModel.loadDataCharts();
        patientModel.loadAbnormalActivities();
    }

    onAnnotate(){
        patientModel.addAnnotation(patientModel.state.annotatedActivity.activityId,
            patientModel.state.annotatedActivity.annotation);
        patientModel.changeAnnotationProperty("activityId", "");
        patientModel.changeAnnotationProperty("annotation", "");
    }

    onRecommend(){
        patientModel.addRecommendation(patientModel.state.recommendationActivity.activityId,
            patientModel.state.recommendationActivity.recommendation);
        patientModel.changeRecommendationProperty("activityId", "");
        patientModel.changeRecommendationProperty("recommendation", "");
    }

    onChangeAnnotation(property, value){
        patientModel.changeAnnotationProperty(property, value);
    }

    onChangeRecommendation(property, value){
        patientModel.changeRecommendationProperty(property, value);
    }

}

const listPatientActivitiesPresenter = new ListPatientActivitiesPresenter();

export default listPatientActivitiesPresenter;
