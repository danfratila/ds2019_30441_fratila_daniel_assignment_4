import patientModel from "../../model/patientModel";

class DeletePatientPresenter {
    onDelete(){
        patientModel.deletePatient(patientModel.state.deleteId)
            .then(() => {
                patientModel.changeDeletePatientProperty("");
                patientModel.loadPatients();
            });
    }

    onChangeDeleteId(value){
        patientModel.changeDeletePatientProperty(value);
    }
}

const deletePatientPresenter = new DeletePatientPresenter();

export default deletePatientPresenter;
