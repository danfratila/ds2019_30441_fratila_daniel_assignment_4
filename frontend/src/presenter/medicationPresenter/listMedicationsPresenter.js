import medicationModel from "../../model/medicationModel";

class ListMedicationsPresenter {
    onInit() {
        medicationModel.loadMedications();
    }
}

const listMedicationsPresenter = new ListMedicationsPresenter();

export default listMedicationsPresenter;
