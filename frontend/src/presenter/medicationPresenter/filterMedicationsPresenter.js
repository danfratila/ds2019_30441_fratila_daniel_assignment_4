import medicationModel from "../../model/medicationModel";

class FilterMedicationsPresenter {
    onFilterByName(){
        medicationModel.clearPreviousFilteredMedications();
        medicationModel.filterByName();
    }

    onChangeFilter(value){
        medicationModel.changeFilterProperty(value);
    }

    onClearFilters(){
        medicationModel.clearFilters();
    }
}

const filterMedicationsPresenter = new FilterMedicationsPresenter();

export default filterMedicationsPresenter;
