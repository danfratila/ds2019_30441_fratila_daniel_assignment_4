import { EventEmitter } from "events";
import RestCaregiver from "../rest/RestCaregiver";

        
class CaregiverModel extends EventEmitter {
    constructor() {
        super();
        this.restCaregiver = new RestCaregiver();
        this.state = {
            caregivers: [],
            newCaregiver: {
                firstname: "",
                lastname: "",
                birthdate: "",
                gender: "",
                address: ""
            },
            updatedCaregiver: {
                caregiverId: "",
                firstname: "",
                lastname: "",
                birthdate: "",
                gender: "",
                address: ""
            },
            filterTag : "",
            filteredCaregivers: [],
            deleteId: ""
        };
    }

    loadCaregivers() {
        return this.restCaregiver.loadAllCaregivers().then(caregivers => {
            console.log(caregivers[0]);
            this.state = {
                ...this.state,
                caregivers: caregivers
            };
            this.emit("change", this.state);
        })
    }


    // --------------------------------------------------------
    // add
    addCaregiver(firstName, lastName, birthdate, gender, address, record){
        return this.restCaregiver.createCaregiver(firstName, lastName, birthdate, gender, address, record)
            .then(caregiver => this.appendCaregiver(caregiver));
    }

    appendCaregiver(caregiver){
        this.state = {
            ...this.state,
            caregivers: this.state.caregivers.concat([caregiver])
        };
        this.emit("change", this.state);
    }

    changeNewCaregiverProperty(property, value) {
        this.state = {
            ...this.state,
            newCaregiver: {
                ...this.state.newCaregiver,
                [property]: value
            }
        };
        this.emit("change", this.state);
    }


    // --------------------------------------------------------
    // filter
    filterByName(){
        return this.restCaregiver.loadCertainCaregiverByName(this.state.filterTag)
            .then(caregivers => {
                this.state = {
                    ...this.state,
                    filteredCaregivers: caregivers
                };
                this.emit("change", this.state);
            });
    }

    changeFilterProperty(value){
        this.state = {
            ...this.state,
            filterTag: value
        };
        this.emit("change", this.state);
    }

    clearPreviousFilteredCaregivers(){
        this.state = {
            ...this.state,
            filteredCaregivers: []
        };
        this.emit("change", this.state);
    }

    clearFilters(){
        this.state = {
            ...this.state,
            filterTag: "",
            filteredCaregivers: []
        };
        this.emit("change", this.state);
    }

    // --------------------------------------------------------
    // update
    changeUpdatedCaregiverProperty(property, value) {
        this.state = {
            ...this.state,
            updatedCaregiver: {
                ...this.state.updatedCaregiver,
                [property]: value
            }
        };
        this.emit("change", this.state);
    }

    updateCaregiver(id, firstName, lastName, birthdate, gender, address, record){
        return this.restCaregiver.updateCaregiver(id, firstName, lastName, birthdate, gender, address, record);
    }


    // --------------------------------------------------------
    // delete
    changeDeleteCaregiverProperty(value){
        this.state = {
            ...this.state,
            deleteId: value
        };
        this.emit("change", this.state);
    }

    deleteCaregiver(id){
        return this.restCaregiver.deleteCaregiverById(id);
    }
}


const caregiverModel = new CaregiverModel();


export default caregiverModel;
