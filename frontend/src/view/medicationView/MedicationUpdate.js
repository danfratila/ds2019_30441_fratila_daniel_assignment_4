import React from "react";

const MedicationUpdate = ({ updatedName, updatedSideEffects, updatedDosage, onUpdateMed, onChange }) => (
    <div>
        <h2>Update medication</h2>
        <div>
            <label>Medication name: </label>
            <input value={updatedName}
                onChange={ e => onChange("name", e.target.value) } />
            <br />
            <label>Side effects: </label>
            <input value={updatedSideEffects}
                onChange={ e => onChange("sideEffects", e.target.value) } />
            <br />
            <label>Dosage per day: </label>
            <input value={updatedDosage}
                onChange={ e => onChange("dosage", e.target.value) } />
            <br />

            <button onClick={onUpdateMed} >Change!</button>

        </div>
    </div>
);

export default MedicationUpdate;
