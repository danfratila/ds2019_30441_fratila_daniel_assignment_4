import React from "react";

const PatientNotificationsList = ({ notifications, title}) => (
    <div>
        <h2>{ title || "Notifications" }</h2>
        <table border="1">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Patient id</th>
                    <th>Activity</th>
                    <th>Start time</th>
                    <th>End time</th>
                </tr>
            </thead>
            <tbody>
                {
                    notifications.map((notification, index) => (
                        <tr key={index} >
                            <td>{notification.id}</td>
                            <td>{notification.patient_id}</td>
                            <td>{notification.activity}</td>
                            <td>{notification.startTime}</td>
                            <td>{notification.endTime}</td>
                        </tr>
                    ))
                }
            </tbody>
        </table>

    </div>
);

export default PatientNotificationsList;
