import React, { Component } from "react";
import patientModel from "../../model/patientModel";

import ActivitiesChart from "./ActivitiesChart";
import AbnormalActivitiesList from "./AbnormalActivitiesList";
import listPatientActivitiesPresenter from "../../presenter/patientPresenter/listPatientActivitiesPresenter"
import logUserPresenter from "../../presenter/userPresenter/logUserPresenter"


const mapModelStateToComponentState = modelState => ({
    activities: modelState.activities,
    title: "Beautiful chart",
    data: modelState.data,
    abnormals: modelState.abnormalActivities,

    activityIdAnnotation: modelState.annotatedActivity.activityId,
    annotation: modelState.annotatedActivity.annotation,

    activityIdRecommendation: modelState.recommendationActivity.activityId,
    recommendation: modelState.recommendationActivity.recommendation
});

export default class SmartActivities extends Component {
    constructor() {
        super();
        this.state = mapModelStateToComponentState(patientModel.state);
        this.listener = modelState => this.setState(mapModelStateToComponentState(modelState));
        logUserPresenter.onValidateLoggedUser();
        patientModel.addListener("change", this.listener);
        listPatientActivitiesPresenter.onInit();
    }

    componentWillUnmount() {
        patientModel.removeListener("change", this.listener);
    }

    render() {
        return (
            <div>
                {/*Validate user every time a page is loaded*/}
                {logUserPresenter.onValidateLoggedUser()}

                <ActivitiesChart
                    activities = {this.state.activities}
                    title = {this.state.title}
                    data={this.state.data}
                />

                <AbnormalActivitiesList
                    activities = {this.state.abnormals}

                    activityIdAnnotation = {this.state.activityIdAnnotation}
                    annotationText = {this.state.annotation}
                    onAnnotate = {listPatientActivitiesPresenter.onAnnotate}
                    onChangeAnnotation = {listPatientActivitiesPresenter.onChangeAnnotation}

                    activityIdRecommendation = {this.state.activityIdRecommendation}
                    recommendationText = {this.state.recommendation}
                    onRecommend = {listPatientActivitiesPresenter.onRecommend}
                    onChangeRecommendation = {listPatientActivitiesPresenter.onChangeRecommendation}

                />
            </div>
        );
    }
}
