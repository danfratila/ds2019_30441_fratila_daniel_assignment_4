import React from "react";

const CaregiverCreate = ({ firstname, lastname, birthDate, gender, caregiverAddress, record, onCreate, onChange }) => (
    <div>
        <h2>Add Caregiver</h2>
        <div>
            <label>First name: </label>
            <input value={firstname}
                   onChange={ e => onChange("firstname", e.target.value) } />
            <br />

            <label>Last name: </label>
            <input value={lastname}
                   onChange={ e => onChange("lastname", e.target.value) } />
            <br />

            <label>birthDate: </label>
            <input value={birthDate}
                onChange={ e => onChange("birthdate", e.target.value) } />
            <br />

            <label>gender: </label>
            <input value={gender}
                onChange={ e => onChange("gender", e.target.value) } />
            <br />

            <label>caregiverAddress: </label>
            <input value={caregiverAddress}
                   onChange={ e => onChange("address", e.target.value) } />
            <br />

            <button onClick={onCreate}>Create!</button>
        </div>
    </div>
);

export default CaregiverCreate;
