import React from "react";

const CaregiverDelete = ({ deleteId, onChangeDeleteId, onDelete}) => (
    <div>
        <h2>{"Delete caregivers"}</h2>
        <label>Delete the caregiver with following id:</label>
        &emsp;
        <input value={deleteId}
               onChange={e => onChangeDeleteId(e.target.value)}/>
        <br/>
        <button className="btn btn-primary" onClick={onDelete}>Delete!</button>

    </div>
);

export default CaregiverDelete;
